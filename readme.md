
#ES6 Up and Running


####Run this in your project:

```sudo npm i -D gulp gulp-sourcemaps vinyl-source-stream vinyl-buffer browserify watchify babelify gulp-babel babel-core babel-preset-es2015```

That installs all the dev ependencies needed to transpile es6.

*I've intentionally left the node_modules in this project though to speed up install, so you don't actually need to add the above.*


**Move the gulpfile to the project**, and either use the package.json file included or add the following babel presets section to your own package.json file

// Package.json

```javascript
"babel": {
  "presets": [
    "es2015"
  ]
},
```

That's really it. Just run `gulp watch` to start watching the src file. Src files transpile to a lib folder. Change the gulpfile if needed.


##Using webpack to transpile with babel and support Common JS modules:
### This is an alternate to the babel transpiling above
*Easier implementation actually. More along the Universal JS methodology lines
```npm install -g webpack```
```npm install --save-dev webpack```
```npm install --save-dev babel-loader```

–Then–

```npm run watch```

