/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var utils = __webpack_require__(1);
	
	/**
	 * SomeModel Class
	 */
	
	var SomeModel = (function () {
		function SomeModel() {
			_classCallCheck(this, SomeModel);
		}
	
		_createClass(SomeModel, [{
			key: 'getAll',
			value: function getAll() {
				return new Promise(function (resolve, reject) {
					console.log('all');
					resolve({ id: 'ejinvpidnadion869' });
				});
			}
		}, {
			key: 'getByID',
			value: function getByID(args) {
				return new Promise(function (resolve, reject) {
					console.log(args.id, 'GET by ID');
					args.name = 'getByID';
	
					resolve(args);
				});
			}
		}, {
			key: 'updateByID',
			value: function updateByID(args) {
				return new Promise(function (resolve, reject) {
					console.log(args.id, 'UPDATE');
	
					resolve(args);
				});
			}
		}]);
	
		return SomeModel;
	})();
	
	var Model = new SomeModel();
	
	/**
	 * Promises
	 */
	Model.getAll().then(function (data) {
		return Model.getByID({ id: data.id });
	}).then(function (data) {
		console.log('DATA1', data);
		return Model.updateByID({ id: data.id });
	}).then(function (data) {
		console.log('DATA2', data);
	}, handleCatch);
	
	function handleCatch(err) {
		console.log(err);
	}
	
	/**
	 * Interpolation
	 * @type {{name: string}}
	 */
	var customer = { name: "Foo" };
	var card = { amount: 7, product: "Bar", unitprice: 42 };
	
	var message = 'Hello ' + customer.name + ',\nwant to buy ' + card.amount + ' ' + card.product + ' for\na total of ' + card.amount * card.unitprice + ' bucks?';
	
	document.getElementById('text').innerText = message;
	
	utils.getAlert();

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';
	
	module.exports = {
		getAlert: function getAlert() {
			alert('got alert');
		}
	};

/***/ }
/******/ ]);
//# sourceMappingURL=app.js.map